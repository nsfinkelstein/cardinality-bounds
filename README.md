# Paper Template Repository

This repository is a template that can be forked for
writing papers.

This assumes that the bib file you want to use lives at

`../references/references.bib`

relative to this directory. It is symlinked to

`writing/references.bib`

where `main.tex` picks it up.
